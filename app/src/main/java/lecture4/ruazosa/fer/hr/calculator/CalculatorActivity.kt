package lecture4.ruazosa.fer.hr.calculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_calculator.*


class CalculatorActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculator)

        var inOperatorMode: Boolean = false
        var inResultMode: Boolean = false

        /* Uzmemo text iz gumba koji je pritisnut
        *  -> Ako nije nista upisano u rezultatu dolje (result_view , vidi XML datoteku)
        *  i ako nije u operacijskom ili rezultatnom mode-u , onda u rezultat dolje nadodaj
        *  vrijednost gumba
        * -> Inace samo dolje upisi vrijednost gumba
        *
        * Zapravo samo upisivanje brojke */
        val numericButtonClicked = {view: View ->
            val buttonValue = (view as Button).text.toString()
            if (!result_view?.text.toString().equals("0") && !inOperatorMode && !inResultMode) {
                    result_view?.text = result_view?.text.toString() + buttonValue
            }
            else {
                inOperatorMode = false
                inResultMode = false
                result_view?.text = buttonValue
            }
        }

        /* Ako pritisnemo jedan od operacijskih gumbova (zbrajanje,dijeljenje itd)
        *  -> Onda u kalkulator se unosi broj koji je upisan u rezultatu
        *  -> u listu operatora se dodaje sto god je na tom gumbu*/
        val operatorButtonClicked = {view: View ->
            val buttonValue = (view as Button).text.toString()
            Calculator.addNumber(result_view?.text.toString())
            Calculator.addOperator(buttonValue)
            inOperatorMode = true
        }

        /* Ako zelimo resetirat (CE tipka ili C ) */
        button_reset?.setOnClickListener {
            Calculator.reset()
            result_view?.setText("0")
        }

        /* Dodavanje zareza
        *  -> Ako vec postoji zarez/tocka , onda ignorira se tipka
        *   -> Inace konkateniramo zarez */
        button_comma?.setOnClickListener {
            if (!result_view?.text.toString().contains(char = '.')) {
                result_view?.text = result_view?.text.toString() + ".";
            }
        }

        /* Dodavanje slušača na gumbove za brojeve*/
        button_zero.setOnClickListener(numericButtonClicked)
        button_one.setOnClickListener(numericButtonClicked)
        button_two.setOnClickListener(numericButtonClicked)
        button_three.setOnClickListener(numericButtonClicked)
        button_four.setOnClickListener(numericButtonClicked)
        button_five.setOnClickListener(numericButtonClicked)
        button_six.setOnClickListener(numericButtonClicked)
        button_seven.setOnClickListener(numericButtonClicked)
        button_eight.setOnClickListener(numericButtonClicked)
        button_nine.setOnClickListener(numericButtonClicked)

        /*Dodavanje slušača na gumbove za operacije */
        button_plus.setOnClickListener(operatorButtonClicked)
        button_minus.setOnClickListener(operatorButtonClicked)
        /* Dodani i za mnozenje i dijeljenje (DR) */
        button_multiply.setOnClickListener(operatorButtonClicked)
        button_divide.setOnClickListener(operatorButtonClicked)

        /* Dodavanje slušača na gumb =
        * -> Ako je u operacijskom načinu rada, dodajemo nulu (objekt kalk interno
        *  izračunaje sve) i iskljucujemo operatorski mod
        * -> Ako nije , onda samo prikazemo rezultat */
        button_evaluate.setOnClickListener {
            if (inOperatorMode) {
                Calculator.addNumber("0")
                inOperatorMode = false
            }
            else {
                Calculator.addNumber(result_view?.text.toString())
            }
            // Izbacujemo vrijednost i resetiramo interne vrijednosti kalkulatora
            Calculator.evaluate()
            result_view?.text = Calculator.result.toString()
            inResultMode = true
            Calculator.reset()
        }


    }
}
