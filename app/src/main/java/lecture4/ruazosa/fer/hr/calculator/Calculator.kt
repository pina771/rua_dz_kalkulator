package lecture4.ruazosa.fer.hr.calculator

import java.util.*

/**
 * Created by dejannovak on 24/03/2018.
 */
object Calculator {

    var result: Double = 0.0
    private set
    var importance:Int = 1 ;

    // Izraz je lista stringova
    // U obliku : br,op,br,op,br,...,br
    var expression: MutableList<String> = mutableListOf()
    private set

    // Resetiranje kalkulatora , postavljamo sve na 0
    fun reset() {
        result = 0.0
        expression = mutableListOf()
    }

    // Dodavanje broja
    fun addNumber(number: String) {
        try {
            val num = number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }

        // Brojevi moraju ici na parna mjesta (mora biti op izmedu brojeva)
        if (expression.count()%2 == 0) {
            expression.add(number)
        }
        else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    // Dodavanje operatora
    fun addOperator(operator: String) {
        if (expression.count()%2 != 1)  {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "/" -> expression.add(operator) // Dodani nasi operatori dijeljenje
            "*" -> expression.add(operator) // mnozenje
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    /* Double Stack metoda evaluacije infix izraza
    *  Koriste se operator stack i operand stack
    *  Temelji se na pracenju vaznosti operatora na vrhu operatorStacka*/
    fun evaluate() {
        // Mora biti paran broj jer izraz ne smije zavrsavati na operator
        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }
        var operandStack: Stack<Double> = Stack<Double>()
        var operatorStack:Stack<String> = Stack<String>()

        for (i in 0..expression.count()-1 ){
            if (i%2 == 0 )
                operandStack.push(expression[i].toDouble())
            else{
                if (operatorStack.isEmpty()){
                    when (expression[i]) {
                        "+" -> operatorStack.push(expression[i])
                        "-" -> operatorStack.push(expression[i])
                        "*" -> {
                            importance = 2
                            operatorStack.push(expression[i])
                        }
                        "/" -> {
                            importance = 2
                            operatorStack.push(expression[i])
                        } } }
                else {
                    if (importance == 1 ) {
                        when (expression[i]) {
                            "+" -> operatorStack.push(expression[i])
                            "-" -> operatorStack.push(expression[i])
                            "*" -> {
                                importance = 2
                                operatorStack.push(expression[i])
                            }
                            "/" -> {
                                importance = 2
                                operatorStack.push(expression[i])
                            } }
                        continue
                    }
                    if (importance == 2 ) {
                        when (expression[i]){
                            "*"->operatorStack.push(expression[i])
                            "/"->operatorStack.push(expression[i])
                            "+"->{
                                while (importance == 2 )
                                    operandStack.push(helpFunction(operatorStack,operandStack))
                                operatorStack.push(expression[i])
                            }
                            "-"->{
                                while(importance == 2)
                                    operandStack.push(helpFunction(operatorStack,operandStack))
                                operatorStack.push(expression[i])
                            }
                        }
                    }
                }
            }

        }
        while(!operatorStack.isEmpty())
            operandStack.push(helpFunction(operatorStack,operandStack))
        importance=1 // Na kraju obnavljamo vaznost
        result = operandStack.pop() // Na dnu
    }

    /* Pomocna funkcija za evaluaciju
    *  Obavlja top of stack operaciju između 2 top of stack elementa i vraca njihov rezultat
    *  Po potrebi azurira vaznost operatorStacka */
    private fun helpFunction(operatorStack:Stack<String>,operandStack:Stack<Double>):Double{
        val A: Double = operandStack.pop()
        val B: Double = operandStack.pop()
        val op: String = operatorStack.pop()
        var retval:Double = 0.0
        when(op) {
            "+" -> retval = A + B
            "-" -> retval = B - A
            "*" -> retval = A * B
            "/" -> {
                if(operatorStack.isEmpty())
                    retval = B / A
                else if (operatorStack.peek() == "/")
                    retval = B * A
                else
                    retval = B / A
            }
        }
        if (operatorStack.isEmpty())
            return retval
        if (operatorStack.peek() == "*" || operatorStack.peek()=="/")
            importance=2
        else
            importance=1
        return retval
    }

}
